#!/bin/bash

set -e

WORKDIR=$AUTOPKGTEST_ARTIFACTS
cd "$WORKDIR"

# Create the cpp file.
cat <<EOF > mandelbrot.cpp
#include "mandelbrot_ispc.h"
#include <iostream>
int main() {
    unsigned int width = 768, height = 512;
    unsigned int midWidth = width/2;
    unsigned int midHeight = height/2;
    float x0 = -2., x1 = 1.;
    float y0 = -1., y1 = 1.;
    int maxIterations = 256;
    int *buf = new int[width*height];

    ispc::mandelbrot_ispc(x0, y0, x1, y1, width, height, maxIterations, buf);
    std::cout << buf[(midWidth-1)*(midHeight-1)] << "\t|";
    std::cout << buf[(midWidth)*(midHeight-1)] << "\t|";
    std::cout << buf[(midWidth+1)*(midHeight-1)] << "\t|" << std::endl;
    std::cout << buf[(midWidth-1)*(midHeight)] << "\t|";
    std::cout << buf[(midWidth)*(midHeight)] << "\t|";
    std::cout << buf[(midWidth+1)*(midHeight)] << "\t|" << std::endl;
    std::cout << buf[(midWidth-1)*(midHeight+1)] << "\t|";
    std::cout << buf[(midWidth)*(midHeight+1)] << "\t|";
    std::cout << buf[(midWidth+1)*(midHeight+1)] << "\t|" << std::endl;

    return 0;
}
EOF

# Create the ispc file.
cat <<EOF > mandelbrot.ispc
static inline int mandel(float c_re, float c_im, int count) {
    float z_re = c_re, z_im = c_im;
    int i;
    for (i = 0; i < count; ++i) {
        if (z_re * z_re + z_im * z_im > 4.)
            break;
        float new_re = z_re*z_re - z_im*z_im;
        float new_im = 2.f * z_re * z_im;
        z_re = c_re + new_re;
        z_im = c_im + new_im;
    }
    return i;
}
export void mandelbrot_ispc(uniform float x0, uniform float y0,
                            uniform float x1, uniform float y1,
                            uniform int width, uniform int height,
                            uniform int maxIterations,
                            uniform int output[])
{
    float dx = (x1 - x0) / width;
    float dy = (y1 - y0) / height;

    for (uniform int j = 0; j < height; j++) {
        foreach (i = 0 ... width) {
            float x = x0 + i * dx;
            float y = y0 + j * dy;

            int index = j * width + i;
            output[index] = mandel(x, y, maxIterations);
        }
    }
}
EOF



echo "building..."
mkdir -p objs
ispc -O2 mandelbrot.ispc -o objs/mandelbrot_ispc.o -h objs/mandelbrot_ispc.h
g++ mandelbrot.cpp -Iobjs/ -O3 -Wall -c -o objs/mandelbrot.o
g++ -Iobjs/ -O3 -Wall -o mandelbrot objs/mandelbrot.o objs/mandelbrot_ispc.o -lm

echo "running..."
test -x mandelbrot
./mandelbrot
